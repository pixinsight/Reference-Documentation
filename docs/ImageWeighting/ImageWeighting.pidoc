\documentclass PIGenericDoc

\document ImageWeighting

\keywords {
   image weighting, quality estimator, robust estimator, SNR, image integration, robust statistics
}

\title {
   New Image Weighting Algorithms in PixInsight
}

\author {
   Juan Conejero, Edoardo Luca Radice and Roberto Sartori (Pleiades Astrophoto, S.L.)
}

\contributor {
   John Pane
}

\copyright {
   2022 Pleiades Astrophoto. All Rights Reserved.
}

\brief {
   A description of the image weighting algorithms currently implemented on the PixInsight platform.
}

%\pragma[noequations]
\pragma[numberequations]

\include 01-Introduction.pidoc
\include 02-PSF_Flux_Weighting_Algorithms.pidoc
\include 03-Implementation.pidoc
\include 04-Examples.pidoc
\include 05-Linear_Regression_Analysis.pidoc
\include References.pidoc

\make
