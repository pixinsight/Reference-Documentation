!<

\section { Examples } {

   In this section we are going to show interesting examples of image weighting with the SubframeSelector tool and the new image weighting algorithms in PixInsight 1.8.9. For readers not familiarized with SubframeSelector, each of these graphs plot two different functions:

      \list[spaced] {
         { \s {Continuous blue line:} The main property, with its name shown at the top left corner and the Y-axis scale at the left side. }
         { \s {Dashed gray line:} The secondary property, with its name shown at the top right corner and the Y-axis scale at the right side. }
      }

   Refer to the implementation subsection on \lref impl_sfs SubframeSelector for a description of the different properties available and how they are related to characteristics of the image.

   For each example we are going to include a set of graphs comparing the new PSF Signal Weight (PSFSW) and PSF SNR estimators with other properties, forming particularly interesting pairs. Here is the list of secondary properties that we are going to use for analysis of the new algorithms:

   \list[spaced] {
      { \s Stars. The number of detectable stars in the image is a robust indicator of sky transparency and signal strength. A comprehensive image quality estimator must show a strong correlation with this property, since it is by itself a primary quality indicator. }
      { \s Altitude. For ground-based observations, the altitude above the horizon at the time of acquisition has a strong influence on transparency conditions, especially for low altitudes below 45 degrees. There are other factors that can mask the repercussion of this property, but some correlation is to be expected. }
      { \s FWHM. This is a robust average of the standard Full Width at Half Maximum function measured for all detected and fitted stars. High FWHM values denote bad seeing, bad focus, bad tracking, or a combination of some or all of these factors. }
      { \s SNR. The standard ratio of powers formulation of \lref snr_estimator {signal-to-noise ratio} is the basis of a maximum likelihood estimator of deterministic signal. Despite the robustness problems associated with the SNR formulation (See Equation \eqnref eqn_snr and the discussion on the referred section), when the SNR property shows no issues a counter-test with it is important to assess the overall reliability of any quality or SNR estimator. }
      { \s Noise. Although the noise estimator is part of the SNR property, as well as of the PSFSW and PSF SNR estimators, a direct comparison can be interesting to discover the actual influence of the noise with respect to other important quality variables. }
      { \s {PSFSW vs PSF SNR.} By mutually comparing our two new estimators we can see how they differ by measuring different image properties. PSFSW is a comprehensive estimator of image quality. PSN SNR is a robust estimator of signal-to-noise ratio. }
   }

   Now that you have complete information on the purpose of our tests, we'll show you the test results to let you draw your own conclusions, pointing out only particularly relevant facts.

   \subsection { \label example_NGC7635_Ha NGC 7635 / H-\alpha } {

      This data set has 69 H-\alpha frames of the Bubble Nebula acquired with a Moravian G3-16200 CCD camera by Edoardo Luca Radice.

      \figure[marginTop:2em] {

         \figtag \s {NGC 7635 / H-\alpha data set: SubframeSelector graphs}

         \imageselect[menupos:right,imageWidth:75\%,imageBorder:1px solid gray] {
            Bolla_1.png { PSFSW / PSF SNR }
            Bolla_2.png { PSFSW / SNR }
            Bolla_3.png { PSFSW / Stars }
            Bolla_4.png { PSFSW / FWHM }
            Bolla_5.png { PSFSW / Altitude }
            Bolla_7.png { PSFSW / Noise }
            Bolla_6.png { PSF SNR / SNR }
         }
      }

      \figure[marginTop:2em] {

         \figtag \s {NGC 7635 / H-\alpha data set: Selection of subframes}

         \block[width:49.4\%,float:left] {
            \image[width:100\%,height:auto] Bolla_frame_33.png
            Frame 33: PSFSW = 0.002209 | PSFSNR = 0.004399
         }
         \block[width:49.4\%,float:right] {
            \image[width:100\%,height:auto] Bolla_frame_3.png
            Frame 3: PSFSW = 0.001443 | PSFSNR = 0.002819
         } \nf
         \block[width:49.4\%,float:left] {
            \image[width:100\%,height:auto] Bolla_frame_57.png
            Frame 57: PSFSW = 0.001298 | PSFSNR = 0.004962
         }
         \block[width:49.4\%,float:right] {
            \image[width:100\%,height:auto] Bolla_frame_51.png
            Frame 51: PSFSW = 6.796\times{}10\sup{-5} | PSFSNR = 1.025\times{}10\sup{-4}
         } \nf
      }

      \figure[marginTop:2em] {

         \block[width:49.4\%,float:left] {
            \image[width:100\%,height:auto] Bolla_frame_33_crop.png
            Frame 33: PSFSW = 0.002209 | PSFSNR = 0.004399
         }
         \block[width:49.4\%,float:right] {
            \image[width:100\%,height:auto] Bolla_frame_57_crop.png
            Frame 57: PSFSW = 0.001298 | PSFSNR = 0.004962
         } \nf

         \figtag Zoomed views of frames 33 (maximum PSFSW) and 57 (maximum PSF SNR). The higher mean FWHM in frame 57 has clearly penalized it for the PSFSW quality estimator. However, the PSF SNR estimator ignores signal concentration and hence is unaffected by star sizes, giving its maximum for frame 57 in this example. Contrarily to what may seem from this comparison, eccentricity (star elongation) is not an important factor here, since our implementation is able to measure total star fluxes by fitting elliptical PSF parameters to the shape and dimensions of each star.
      }
   }

   \subsection { \label example_M45_red M45 / Red Channel } {

      A set of 121 frames of M45 acquired with a Moravian G3-16200 CCD camera through a red filter by Edoardo Luca Radice. This set is interesting for quality estimation tests because it includes a small subset of images acquired under quite poor transparency conditions.

      \figure[marginTop:2em] {

         \figtag \s {M45 / Red data set: SubframeSelector graphs}

         \imageselect[menupos:right,imageWidth:75\%,imageBorder:1px solid gray] {
            M45_1.png { PSFSW / PSF SNR }
            M45_2.png { PSFSW / SNR }
            M45_3.png { PSFSW / Stars }
            M45_4.png { PSFSW / FWHM }
            M45_5.png { PSFSW / Altitude }
            M45_6.png { PSFSW / Noise }
            M45_7.png { PSF SNR / SNR }
         }
      }

      \figure[marginTop:2em] {

         \figtag \s {M45 / Red data set: Selection of subframes}

         \block[width:49.4\%,float:left] {
            \image[width:100\%,height:auto] M45_frame_17.png
            Frame 17: PSFSW = 0.605 | PSFSNR = 2.391
         }
         \block[width:49.4\%,float:right] {
            \image[width:100\%,height:auto] M45_frame_1.png
            Frame 1: PSFSW = 0.537 | PSFSNR = 2.113
         } \nf
         \block[width:49.4\%,float:left] {
            \image[width:100\%,height:auto] M45_frame_115.png
            Frame 115: PSFSW = 0.254 | PSFSNR = 1.847
         }
         \block[width:49.4\%,float:right] {
            \image[width:100\%,height:auto] M45_frame_29.png
            Frame 29: PSFSW = 2.405\times{}10\sup{-7} | PSFSNR = 2.199\times{}10\sup{-7}
         } \nf
      }

      \figure[marginTop:2em] {

         \imageselect[menupos:bottom,imageWidth:100\%,imageBorder:1px solid gray] {
            M45_frame_17_crop.png { Frame 17: PSFSW = 0.605 | PSFSNR = 2.391 }
            M45_frame_115_crop.png { Frame 115: PSFSW = 0.254 | PSFSNR = 1.847 }
            M45_frame_17_crop_dpsf.png { Frame 17 + fitted PSFs }
            M45_frame_115_crop_dpsf.png { Frame 115 + fitted PSFs }
         } \nf

         \figtag Zoomed views of frames 17 (maximum PSFSW) and 115. The visible difference in image resolution between these two frames causes a substantial difference in PSFSW estimates (605 and 0.254, respectively), although not so much in PSF SNR estimates (2.391 and 1.847, respectively).
      }

      It is evident that the SNR estimator is not working correctly \e {as a quality estimator} for this data set. This is because the strong gradients in the subset of bad frames (frames 26--50) are being interpreted as signal by the SNR equation (see Equation \eqnref eqn_snr). The problem is not that SNR is wrong, but that SNR is not the appropriate estimator for this data set. In general, SNR cannot be used for image weighting for sets with differing strong gradients, as is the case in this example.
   }

   \subsection { M81-M82 / Green Channel } {

      A small set of 16 wide field frames of the M81/M82 region, acquired with a Finger Lakes ProLine PL16801 CCD camera through a green filter. The set was acquired under excellent sky conditions at the Astronomical Observatory of Aras de los Olmos (OAO), University of Valencia, Spain. Altitude above the horizon is in this case the main factor determining image quality.

      \figure[marginTop:2em] {

         \figtag \s {M81-M82 / Green data set: SubframeSelector graphs}

         \imageselect[menupos:right,imageWidth:75\%,imageBorder:1px solid gray] {
            M81M82_1.png { PSFSW / PSF SNR }
            M81M82_2.png { PSFSW / SNR }
            M81M82_3.png { PSFSW / Stars }
            M81M82_4.png { PSFSW / FWHM }
            M81M82_5.png { PSFSW / Altitude }
            M81M82_6.png { PSFSW / Noise }
            M81M82_7.png { PSF SNR / SNR }
         }
      }

      \figure[marginTop:2em] {

         \figtag \s {M81-M82 / Green data set: Selection of subframes}

         \block[width:49.4\%,float:left] {
            \image[width:100\%,height:auto] M81M82_frame_1.png
            Frame 1: PSFSW = 3.741 | PSFSNR = 9.037
         }
         \block[width:49.4\%,float:right] {
            \image[width:100\%,height:auto] M81M82_frame_6.png
            Frame 6: PSFSW = 3.070 | PSFSNR = 6.594
         } \nf
         \block[width:49.4\%,float:left] {
            \image[width:100\%,height:auto] M81M82_frame_11.png
            Frame 11: PSFSW = 2.458 | PSFSNR = 4.362
         }
         \block[width:49.4\%,float:right] {
            \image[width:100\%,height:auto] M81M82_frame_16.png
            Frame 16: PSFSW = 2.130 | PSFSNR = 3.865
         } \nf
      }

      As expected, without other factors besides the natural variation in sky transparence due to the decreasing altitude, the PSF SNR and SNR estimators show high similarity.
   }

   \subsection { M8 / OIII } {

      A large set of 158 wide field frames of the M8 region, acquired with a STC Astro Duo-Narrowband filter and a ZWO ASI2600MC Pro OSC camera by Bernd Landmann. The data used for this example corresponds to the green channel of the demosaiced images, which captures most of the OIII line emission. These images have been acquired under excellent conditions from La Palma, Canary Islands, Spain.

      \figure[marginTop:2em] {

         \figtag \s {M8 / OIII data set: SubframeSelector graphs}

         \imageselect[menupos:right,imageWidth:75\%,imageBorder:1px solid gray] {
            M8_1.png { PSFSW / PSF SNR }
            M8_2.png { PSFSW / SNR }
            M8_3.png { PSFSW / Stars }
            M8_4.png { PSFSW / FWHM }
            M8_5.png { PSFSW / Altitude }
            M8_6.png { PSFSW / Noise }
            M8_7.png { PSF SNR / SNR }
         }
      }

      \figure[marginTop:2em] {

         \figtag \s {M8 / OIII data set: Selection of subframes}

         \block[width:49.4\%,float:left] {
            \image[width:100\%,height:auto] M8_frame_66.png
            Frame 66: PSFSW = 1.8367 | PSFSNR = 1.3783
         }
         \block[width:49.4\%,float:right] {
            \image[width:100\%,height:auto] M8_frame_144.png
            Frame 144: PSFSW = 0.9845 | PSFSNR = 1.4237
         } \nf
         \block[width:49.4\%,float:left] {
            \image[width:100\%,height:auto] M8_frame_19.png
            Frame 19: PSFSW = 1.2393 | PSFSNR = 0.7481
         }
         \block[width:49.4\%,float:right] {
            \image[width:100\%,height:auto] M8_frame_121.png
            Frame 121: PSFSW = 0.0275 | PSFSNR = 0.0262
         } \nf
      }

      \figure[marginTop:2em] {

         \imageselect[menupos:bottom,imageWidth:100\%,imageBorder:1px solid gray] {
            M8_frame_66_crop.png { Frame 66: PSFSW = 1.8367 | PSFSNR = 1.3783 }
            M8_frame_146_crop.png { Frame 146: PSFSW = 0.6813 | PSFSNR = 1.5126 }
         } \nf

         \figtag Comparison between the frames with the highest PSFSW (66) and the highest PSF SNR (146) estimates. FWHM is the determinant factor here between a comprehensive estimator of image quality and a robust estimator of signal-to-noise ratio.
      }
   }

   \subsection {\label example_sh2280 SH2-280 / H-\alpha} {

      In this example, we would discuss how the new image weighting algorithms balance image quality factors differently. The dataset consists of 74 light frames, with exposures of 600s and an H-\alpha filter on the SH2--280 emission nebula. The dataset was taken in two separate nights where sky conditions visibly changed, and the passage of some clouds ruined the quality of a few frames. As usually happens under urban skies, image brightness due to light pollution changed significantly across the two nights. By plotting the image Median vs Altitude, we highlight how the overall image brightness evolved during the two sessions:

      \figure[numbered: example_sh2280_sky_brightness] {

         \figtag \s { SH2-280 / H-\alpha data set: SubframeSelection Median vs Altitude graph }

         \group {
            \image [float:left,marginright:10px,width:620px] SS_median_vs_altitude.png

            Image median value indicates the overall sky brightness. The first night was affected by the arrival of clouds that perturbed some frames in the middle of the session and completely ruined the last images (26--27--28). The sky was almost transparent on the second night, but light pollution appeared slightly higher than the first one.
         }

         \group {
            \image [float:left,marginright:10px,width:200px] Blink_016.png
            \image [float:left,marginright:10px,width:200px] Blink_027.png
            \image [float:left,marginright:10px,width:200px] Blink_031.png

            The light frames are spotted by black circles above with the same auto-stretch applied. On the left, the darkest image of the first session; in the middle one of the frames contaminated by the arrival of clouds; on the right, the darkest image of the second session.
         }
      }

      Sky brightness acts as an additive light source that comes with its unavoidable shot noise: the higher the brightness, the higher the noise. This correlation is visible by depicting Noise vs Median:

      \figure[numbered: example_sh2280_noise_vs_median] {

         \figtag \s { SH2-280 / H-\alpha data set: SubframeSelection Median vs Noise graph }

         \image [float:left,marginright:10px,width:620px] SS_median_vs_noise.png

         Image noise correlates to the median value as expected when the variation of the mean value is due to the presence of an additive light source, as sky brightness.
      }

      The impact of that additive brightness in terms of noise can be easily spotted by blinking the normalized images.

      \figure[numbered: example_sh2280_noise_blink ] {

         \figtag A comparison between the registered/normalized two best frames of each night and a third frame strongly contaminated by the presence of clouds. The frames have been normalized with the LocalNormalization tool to make the visual noise comparison meaningful. While the noise of the best frames is visually similar, the contaminated frames are heavily dominated by sky shot noise.

         \imageselect[menupos:right, imageWidth:620px] {
            Blink_016_noise.png { Session 1 - darkest frame }
            Blink_031_noise.png { Session 2 - darkest frame }
            Blink_027_noise.png { Session 1 - contaminated frame }
         }
      }

      PSF Flux and PSF Total Mean Flux measure the amount of star signal captured and how much that signal is concentrated. Showing the plot of these two properties and other quality measures highlights how powerful both are in capturing different aspects of image quality.

      \figure[numbered: example_sh2280_PSF_Flux_blink ] {

         \figtag A comparison of the stars between the registered best frames of the sessions and a frame strongly contaminated by clouds.

         \imageselect[menupos:right, imageWidth:620px] {
            SS_PSFFlux_vs_FWHM.png { PSF Flux / FWHM }
            SS_PSFFlux_vs_median.png { PSF Flux / Median }
            SS_PSFFlux_vs_stars.png { PSF Flux / Stars }
            SS_PSFFlux_vs_eccentricity.png { PSF Flux / Eccentricity }
         }
      }

      We identify a few significant phases within the whole session, where some PSF Flux properties can be highlighted.

      \figure[numbered: example_sh2280_PSF_Flux_phase_1 ] {

         \figtag \s { SH2-280 / H-\alpha data set: Phase 1, clear sky with improving seeing. }

         \group {
            \imageselect[menupos:right, imageWidth:620px] {
               SS_PSFFlux_vs_FWHM_P1.png { PSF Flux / FWHM }
               SS_PSFFlux_vs_median_P1.png { PSF Flux / Median }
               SS_PSFFlux_vs_stars_P1.png { PSF Flux / Stars }
            }

            Sky brightness regularly lowers as altitude increases. Almost no clouds contaminate the sky, thus FWHM is a good indicator of seeing and scope thermal adaptation. During the initial 60 minutes, FWHM lowers and the faintest stars become less blurry, increasing the number of detected stars accordingly. PSF Flux remains stable, indicating that sky transparency has not changed significantly.
         }

         \group[marginLeft: 40px]{
            \imageselect[menupos:right, imageWidth:580px] {
               Blink_first_session_1.png { First image of Phase 1 }
               Blink_last_session_1.png { Last image of Phase 1 }
               Blink_first_session_1_stars.png { Stars of first image of Phase 1 }
               Blink_last_session_1_stars.png { Stars of last image of Phase 1 }
            }
         }
      }

      \figure[numbered: example_sh2280_PSF_Flux_phase_2 ] {

         \figtag \s { SH2-280 / H-\alpha data set: Phase 2, clouds arrival. }

         \imageselect[menupos:right, imageWidth:620px] {
            SS_PSFFlux_vs_FWHM_P2.png { PSF Flux / FWHM }
            SS_PSFFlux_vs_median_P2.png { PSF Flux / Median }
            SS_PSFFlux_vs_stars_P2.png { PSF Flux / Stars }
         }

         Clouds partially scatter and reflect the star's signal, reducing the overall flux received by the imaging sensor. In this second phase, some clouds have passed, and this is confirmed by inspecting the correlation between three factors: PSF Flux, number of detected stars, and sky median value. The strong correlation between these three factors hints that the source of the brightness increase should also be responsible for the star flux reduction and the reduction in the number of stars detected, all aspects following the presence of clouds.
      }

      \figure[numbered: example_sh2280_PSF_Flux_phase_3 ] {

         \figtag \s { SH2-280 / H-\alpha data set: Phase 3, clouds ruined the images. }

         \imageselect[menupos:right, imageWidth:620px] {
            SS_PSFFlux_vs_FWHM_P3.png { PSF Flux / FWHM }
            SS_PSFFlux_vs_median_P3.png { PSF Flux / Median }
            SS_PSFFlux_vs_stars_P3.png { PSF Flux / Stars }
         }

         In this phase, the last frames of the first night have been strongly deteriorated by the presence of clouds. Images are dominated by the scatter noise and all image quality indicators like FWHM, number of stars, PSF Flux, and median coherently tend towards the worst values. In this situation, the weak correlation between FWHM and PSF Flux suddenly becomes strong.
      }

      \figure[numbered: example_sh2280_PSF_Flux_phase_4 ] {

         \figtag \s { SH2-280 / H-\alpha data set: Phase 4, a night with a clear sky. }

         \imageselect[menupos:right, imageWidth:620px] {
            SS_PSFFlux_vs_FWHM_P4.png { PSF Flux / FWHM }
            SS_PSFFlux_vs_median_P4.png { PSF Flux / Median }
            SS_PSFFlux_vs_stars_P4.png { PSF Flux / Stars }
         }

         This phase corresponds to the entire second night of the shooting. This phase is characterized by the absence of clouds, which provides the ideal condition to inspect how the PSF Flux measure works under a clear sky. As expected, star flux depends on sky transparency, with a weak dependency on small seeing fluctuations. This is confirmed by the graphs, which show a very regular and smooth behaviour of PSF Flux values that are mainly correlated to the part of the sky pointed during the session and to the correspondent atmospheric properties. Given the ideal conditions, we assume FWHM is a good indicator of seeing: we can see that the seeing has a stable trend around 2.4 px, with some fluctuations that are quite uncorrelated to PSF Flux.
      }

      The above analysis on PSF Flux can be summarized with the following statements:

      \list[spaced] {
         { The PSF Flux property shows a strong correlation with sky transparency (atmospheric absorption and presence of clouds) and a weak correlation with seeing (FWHM) }
         { The presence of strong clouds affecting the images turns the weak correlation between PSF Flux and FWHM into a strong correlation. }
      }

      PSF Total Mean Flux measures how much star flux is concentrated into the inner part of the PSF, thus being related to image definition: the higher its value, the sharper the stars. By plotting PSF Total Mean Flux vs FWHM the strong correlation between both properties becomes evident:

      \figure[numbered: example_sh2280_PSF_Total_Mean_Flux_vs_FWHM ] {

         \figtag \s { SH2-280 / H-\alpha data set: PSF Total Mean Flux vs FWHM. }

         \image [float:left,marginright:10px,width:620px] SS_PSFTotalMEanFlux_vs_FWHM.png

         The strong inverse correlation between PSF Total Mean Flux and FWHM is obvious.

         Although this is generally true, when FWHM is reduced because of the presence of clouds which reduce the overall star flux, that inverse correlation becomes a positive one, and the two measures show the same trends, as happens in frame 28.
      }

      We have now all the ingredients to properly discuss how the different weighting algorithms work with the images of this data set:

      \figure[numbered: example_sh2280_PSWSW_vs_PSF_SNR ] {

         \figtag \s { SH2-280 / H-\alpha data set: PSF Signal Weight vs PSF SNR. }

         \image [float:left,marginright:10px,width:620px] SS_PSFSW_vs_PSFSNR.png

         PSF Signal Weight and PSF SNR show an overall good correlation; in particular, \s {both penalize images where star flux is strongly reduced because of the presence of the clouds}. The latter is a very new behaviour that is not taken into account by a standard SNR weighting algorithm. The two methods differ by the PSF Total Mean Flux's role in the PSF Signal Weight formula.

         PSF Signal Weight penalizes more frames from 1 to 8 because \s { they have a higher FWHM }, while PSF SNR takes into account only the overall total star flux.          PSF Signal Weight also fluctuates accordingly with the inverse trend of FWHM in the second night, while PSF SNR closely follows the trend of the overall star flux.
      }

      The comparison between PSF Signal Weight and PSF SNR shows the advantages provided by both methods, along with the differences:

      \list[unordered] {
         { Both PSF Signal Weight and SNR penalize images affected by clouds, where the star flux is strongly reduced. }
         { PSF SNR relies on the overall star flux and is less sensitive to how much the signal is concentrated. }
         { PSF Signal Weight takes into account signal concentration (i.e. definition) and promotes image with higher resolution. }
      }

      Depending on the objective, one weighting method could be preferable against the other.

      Finally, SNR represents the legacy SNR weighting method based on the overall signal-to-noise ratio. The method can be adopted with images that are not contaminated by clouds since the overall signal measurement cannot distinguish between the target and the sky signal, resulting in higher weights assigned to degraded images, as depicted in the following comparison graph:

      \figure[numbered: example_sh2280_PSWSW_vs_SNR ] {

         \figtag \s { SH2-280 / H-\alpha data set: PSF Signal Weight vs SNR. }

         \image [float:left,marginright:10px,width:620px] SS_PSFSW_vs_SNR.png

         SNR is based on the measurement of the image's overall signal. Being positively affected by gradients and sky brightness, the result keeps promoting brighter images where the actual target signal-to-noise ratio is low. Moreover, SNR cannot reject very bright images where the target signal is entirely buried under the noise.
      }

      The PSF Signal Weight algorithm is capable of synthesizing different quality factors into a single variable, balancing both signal-to-noise ratio and image definition. PSF SNR is a robust alternative to the standard SNR weighting algorithm.

      Both methods are capable of capturing and penalizing images that have been deteriorated by the presence of clouds, overcoming the limitations of a purely SNR-based weighting method.
   }
}
