#include <pjsr/UndoFlag.jsh>

#define sampleSize      4096
#define numberOfSamples 10000

function bootstrap( estimator )
{
   console.show();
   console.writeln( "<end><cbr>Performing bootstrap, please wait..." );
   console.flush();

   let P = new PixelMath;
   P.expression = "gauss()";
   P.rescale = true;
   P.truncate = false;

   let window = new ImageWindow( sampleSize, sampleSize, 1, 32/*bps*/, true/*float*/, false/*color*/ );
   let view = window.mainView;
   view.beginProcess( UndoFlag_NoSwapFile );
   let image = view.image;

   let R = new Float32Array( numberOfSamples );
   let S = new Float32Array( numberOfSamples );
   let K = new Float32Array( numberOfSamples );

   let T = new ElapsedTime;

   for ( let i = 0; i < numberOfSamples; ++i )
   {
      P.executeOn( view, false/*swapFile*/ );
      processEvents();
      let r = image.stdDev();
      let s = estimator( image );
      R[i] = r;
      S[i] = s;
      K[i] = r/s;
   }

   let k = Math.median( K );
   for ( let i = 0; i < numberOfSamples; ++i )
      S[i] *= k;
   let var_r = Math.variance( R );
   let var_s = Math.variance( S );

   view.endProcess();
   window.forceClose();

   console.writeln( format( "<end><cbr><br>Variance of stddev ...... %.4e", var_r ) );
   console.writeln( format(               "Variance of estimator ... %.4e", var_s ) );
   console.writeln( format(               "Gaussian efficiency ..... %.3f", var_r/var_s ) );
   console.writeln( format(               "Normalization factor .... %.5f", k ) );
   console.writeln( T.text );
}

/*
 * N* noise estimator - MAD variant
 */
function NStar_MAD( image )
{
   return Math.MAD( image.mmtBackgroundResidual( 256 ) );
}

/*
 * N* noise estimator - Sn variant
 */
function NStar_Sn( image )
{
   return Math.Sn( image.mmtBackgroundResidual( 256 ) );
}

bootstrap( NStar_Sn );
