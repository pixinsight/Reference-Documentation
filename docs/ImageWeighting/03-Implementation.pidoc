!<

\section { Implementation } {

   In this section we describe the current implementation of the new image weighting algorithms in several important processes of our standard data reduction and image preprocessing pipeline. We describe the relevant parameters available in several tools and provide practical usage information.

   It is important to point out that the actual image weights are not stored in the images generated by any of the tools in our preprocessing pipeline. All of these tools generate and/or use specific metadata to compute the weights when necessary. At the end of this section we describe also the metadata currently generated for the application of these algorithms.

   \subsection { ImageCalibration and Debayer } {

      The ImageCalibration and Debayer tools have two new sections since version 1.8.9 of PixInsight, namely \e {Signal Evaluation} and \e {Noise Evaluation,} which provide most of the necessary parameters to control the new PSF flux based image weighting algorithms.

      \note {

         \s Important--- It cannot be overemphasized that signal and noise evaluation must be carried out by different processes, depending on the type of raw data:

         \list {

            { \s { Monochrome CCD or CMOS raw frames: } signal and noise evaluation must be performed by the ImageCalibration process. }

            { \s { Mosaiced CFA raw frames: } signal and noise evaluation must be performed by the Debayer process. Debayer will always compute noise estimates before demosaicing, that is, from uninterpolated raw calibrated data. }
         }

         If you use our standard WeightedBatchPreprocessing script to calibrate your data, and to demosaic it when appropriate, then the correct procedures will always be applied.
      }

      \subsection { Signal Evaluation } {

         \figure {

            \image[width:493px,height:auto,border:1px solid gray,marginRight:32px,float:left] IC_SignalEvaluation.png

            \figtag The Signal Evaluation section of the ImageCalibration and Debayer tools.
         }

         Many of the parameters in this section provide control over the initial star detection phase. In most cases there is no need to change the values of these parameters, since our star detection routines are robust even under quite unfavorable conditions. However, in particularly problematic cases the availability of these parameters can become very useful.

         \definition {

            { Detection scales } {

               Our star detection routines implement a multiscale algorithm. This is the number of wavelet layers used for structure detection. With more wavelet layers, larger stars, but perhaps also some nonstellar objects, will be detected.
            }

            { Saturation threshold } {

               Detected stars with one or more pixels with values above this threshold will be excluded for signal evaluation. This parameter is expressed in the \[0,1\] range. It can be applied either as an absolute pixel sample value in the normalized \[0,1\] range, or as a value relative to the maximum pixel sample value of the measured image (see the \e {Relative saturation threshold} parameter below).

               The default saturation threshold is 1.0. For signal evaluation, the implemented star detection and outlier rejection routines are normally able to avoid contamination from saturated sources, so the default value of this parameter should not be changed under normal conditions.
            }

            { Relative saturation threshold } {

               The \e {saturation threshold} parameter, when it is set to a value less than one, can be applied either as an absolute pixel sample value in the normalized \[0,1\] range, or as a value relative to the maximum pixel sample value of the measured image. The relative saturation threshold option is enabled by default.
            }

            { Noise scales } {

               Number of wavelet layers used for noise reduction. Noise reduction prevents detection of bright noise structures as false stars, including hot pixels and cosmic rays. This parameter can also be used to control the sizes of the smallest detected stars (increase it to exclude more stars), although the \e {minimum structure size} parameter can be more efficient for this purpose.
            }

            { Minimum structure size } {

               Minimum size of a detectable star structure in square pixels. This parameter can be used to prevent detection of small and bright image artifacts as stars, when such artifacts cannot be removed with a median filter (i.e., with the \e {hot pixel removal} parameter).

               Changing the default zero value of this parameter should not be necessary with correctly acquired and calibrated data. It may help, however, when working with poor quality data such as poorly tracked, poorly focused, wrongly calibrated, low-SNR raw frames, for which our star detection algorithms have not been designed specifically.
            }

            { Hot pixel removal } {

               Size of the hot pixel removal filter. This is the radius in pixels of a median filter applied by the star detector before the structure detection phase. A median filter is very efficient to remove \e {hot pixels}. If not removed, hot pixels will be identified as stars, and if present in large amounts, can prevent a valid signal evaluation. To disable hot pixel removal, set this parameter to zero.
            }

            { Noise reduction } {

               Size of the noise reduction filter. This is the radius in pixels of a Gaussian convolution filter applied to the working image used for star detection. Use it only for very low SNR images, where the star detector cannot find reliable stars with its default parameters. To disable noise reduction, set this parameter to zero.
            }

            { PSF type } {

               Point spread function type used for PSF fitting and photometry. In all cases elliptical functions are fitted to detected star structures, and PSF sampling regions are defined adaptively using a median stabilization algorithm.

               When the \e Auto option is selected, a series of different PSFs will be fitted for each source, and the fit that leads to the least absolute difference among function values and sampled pixel values will be used for flux measurement. Currently the following functions are tested in this special automatic mode: Moffat functions with \beta shape parameters equal to 2.5, 4, 6 and 10.

               The rest of options select a fixed PSF type for all detected sources, which improves execution times at the cost of a less adaptive, and hence potentially less accurate, signal estimation process.
            }

            { Growth factor } {

               Growing factor for expansion/contraction of the PSF flux measurement region for each source, in units of the Full Width at Tenth Maximum (FWTM).

               The default value of this parameter is 1.0, meaning that flux is measured exclusively for pixels within the elliptical region defined at one tenth of the fitted PSF maximum. Increasing this parameter can improve accuracy of PSF flux measurements for undersampled images, where PSF fitting uncertainty is relatively large. Decreasing it can be beneficial in some cases working with very noisy data to restrict flux evaluation to star cores.
            }

            { Maximum stars } {

               The maximum number of stars that can be measured to compute PSF flux estimates. PSF photometry will be performed for no more than the specified number of stars. The subset of measured stars will always start at the beginning of the set of detected stars, sorted by brightness in descending order.

               The default value imposes a generous limit of 24K stars. Limiting the number of photometric samples can improve performance for calibration of wide-field frames, where the number of detected stars can be very large. However, reducing the set of measured sources too much will damage the accuracy of signal estimation.
            }
         }
      }

      \subsection { Noise Evaluation } {

         \figure {

            \image[width:493px,height:auto,border:1px solid gray,marginRight:32px,float:left] IC_NoiseEvaluation.png

            \figtag The Noise Evaluation section of the ImageCalibration and Debayer tools.
         }

         This section allows you to control the noise evaluation algorithm used for generation of PSF flux based metadata, which in turn is used in the calculation of PSF Signal Weight and PSF SNR estimates.

         \definition {

            { Evaluation algorithm } {

               This option selects an algorithm for automatic estimation of the standard deviation of the noise in the calibrated/demosaiced images.

               The \e { multiresolution support } (MRS) noise estimation routine implements the iterative algorithm described by Jean-Luc Starck and Fionn Murtagh in their 1998 paper \e {Automatic Noise Estimation from the Multiresolution Support}.\ref starck_1998 In our implementation, the standard deviation of the noise is evaluated on the first four wavelet layers, assuming a Gaussian noise distribution. MRS is a remarkably accurate and robust algorithm and the default option for noise evaluation.

               The \e {iterative k-sigma clipping} algorithm can be used as a last-resort option in cases where the MRS algorithm does not converge systematically. This can happen on images with no detectable small-scale noise, but should never happen with raw or calibrated data under normal working conditions, so this option is available more for completeness than for practical reasons.

               The \e {N* robust noise estimator} extracts a subset of residual pixels by comparison with a large-scale model of the local background of the image, generated with the multiscale median transform (MMT). Since the MMT is remarkably efficient at isolating image structures, it can be used to detect pixels that cannot contribute to significant structures. N* is an accurate and robust, alternative estimator of the standard deviation of the noise that does not assume or require any particular statistical distribution in the analyzed data.
            }
         }
      }
   }

   \subsection { \label impl_sfs SubframeSelector } {

      The SubframeSelector tool was written by Cameron Leger in 2017, based on the homonym script written by Mike Schuster, back in 2013. Both PTeam members made fundamental contributions to the PixInsight platform with this powerful image analysis infrastructure of unparalleled efficiency and versatility. Since the initial release of the SubframeSelector tool we have been improving it at a constant rate to incorporate new advances in image analysis developed on the PixInsight platform. Obviously, development of the new image quality estimators subject of the present article is no exception. Since version 1.8.9 of PixInsight, the new PSF Signal Weight and PSF SNR estimators are tightly integrated with our standard preprocessing pipeline, including the SubframeSelector tool as an essential actor.

      \figure {

         \image[width:800px,height:auto] SFS.png

         \figtag The SubframeSelector tool showing PSFSW and PSF SNR graphs for a data set.
      }

      The SubframeSelector tool supports a new set of variables (also called \e {properties}) for image selection and weighting, as well as for subframe sorting and graph generation. Refer to the reference documentation of this tool for complete information. The following list includes all supported variables for completeness of reference. In all cases the value of each variable is computed separately for each measured subframe.

      \definition {
         { Altitude } {

            The altitude above the horizon for the equatorial coordinates of the center of the subframe at the time of acquisition, in the range \[0\deg,+90\deg\].
         }
         { Azimuth } {

            The azimuth for the equatorial coordinates of the center of the subframe at the time of acquisition, in the range \[0\deg,360\deg).
         }
         { Eccentricity } {

            A robust mean of the set of measured eccentricities from all fitted PSFs.
         }
         { EccentricityMeanDev } {

            The mean absolute deviation from the median of the set of Eccentricity values.
         }
         { FWHM } {

            A robust mean of the set of full width at half maximum (FWHM) measurements for all fitted PSFs.
         }
         { FWHMMeanDev } {

            The mean absolute deviation from the median of the set of FWHM values.
         }
         { Index } {

            The one-based index of the subframe, in the \[1,\e{n}\] range, where \e n is the number of measured subframes.
         }
         { Median } {

            The median pixel sample value.
         }
         { MedianMeanDev } {

            The mean absolute deviation from the median.
         }
         { MStar } {

            The \im {#: M^\star :#} robust mean background estimate.
         }
         { Noise } {

            The noise estimate. This is the MRS estimate by default, unless a different option has been selected in the ImageCalibration or Debayer tools.
         }
         { NoiseRatio } {

            The fraction of pixels used for noise evaluation with the MRS algorithm, in the \[0,1\] range. Always zero when the \im {#: N^\star :#} algorithm is used to compute noise estimates.
         }
         { NStar } {

            The \im {#: N^\star :#} robust noise estimate.
         }
         { PSFCount } {

            The number of PSF fits used for PSF flux evaluation.
         }
         { PSFFlux } {

            The sum of all PSF flux estimates.
         }
         { PSFFluxPower } {

            The sum of all PSF squared flux estimates. Currently not used, reserved for future extensions.
         }
         { PSFSignalWeight } {

            The PSF Signal Weight (PSFSW) estimate of image quality.
         }
         { PSFSNR } {

            The PSF SNR estimate.
         }
         { PSFTotalMeanFlux } {

            The sum of all PSF mean flux estimates.
         }
         { PSFTotalMeanPowerFlux } {

            The sum of all PSF mean squared flux estimates. Currently not used, reserved for future extensions.
         }
         { SNR } {

            The SNR estimate.
         }
         { SNRWeight } {

            An alias for the SNR estimate, for backward compatibility.
         }
         { StarResidual } {

            A robust mean of the set of MAD estimates for all fitted PSFs. Each MAD estimate is a Winsorized mean absolute deviation of fitted function values with respect to pixel values measured on the PSF sampling region.
         }
         { StarResidualMeanDev } {

            The mean absolute deviation from the median for the set of StarResidual values.
         }
         { Stars } {

            The total number of sources detected and fitted by the star detection and PSF fitting routines integrated in the SubframeSelector tool.
         }
      }

      Each variable (excluding \e {Index}) also has a Median, Min and Max version (e.g. \e {FWHMMin}), which are computed across all subframes. Each variable (excluding \e {Index}) also has a Sigma version (e.g. \e {PSFSignalSigma}), where the value is expressed in sigma units of the mean absolute deviation from the median.

      \note { \s Important--- SubframeSelector expects all signal and noise evaluation metadata items to be present in its input subframes. This can only happen if the subframes have been calibrated, and demosaiced when appropriate, with current versions of our ImageCalibration and Debayer tools. When signal and/or noise evaluation metadata items are not available for a subframe, the SubframeSelector tool computes them directly from input subframe pixels. In such case, if the subframe has been interpolated in some way (for example, after demosaicing and/or image registration), the computed items can be inaccurate (especially noise estimates) and hence the derived PSFSW and PSF SNR values can also be inaccurate, or even wrong in extreme cases. Appropriate warning messages are shown when this happens. }
   }

   \subsection { ImageIntegration and DrizzleIntegration } {

      Since version 1.8.9 of PixInsight our standard ImageIntegration tool supports the new PSF Signal Weight (PSFSW) and PSF SNR estimators for calculation of image weights. The weighted average combination of \im n images can be represented by the equation

      \equation { #:
      $$
      I_{wavg}(x,y)=\frac{\sum_{i=1}^n w_i I_i(x,y) }{\sum_{i=1}^n w_i}
      $$
      :# }

      for each non-rejected pixel at \im (x,y) coordinates, where \im w_i is the weight assigned to the image \im I_i. As we have explained in the introduction of this document, different weights can be assigned to maximize (or minimize) different properties of the integrated image. When the PSFSW estimator is selected for weighting, the integration is optimized for a balanced set of image quality variables, including metrics representing SNR and image resolution; the ability of PSFSW to capture important quality metrics will be analyzed in the \lref linear_regression_analysis {Linear Regression Analysis} section. When the PSF SNR estimator is used, the integration tends to be optimized for maximization of SNR in the integrated result.

      \figure {

         \image[width:501px,height:auto,border:1px solid gray,marginRight:32px,float:left] II_Weights.png

         \figtag The \e Weights parameter of the ImageIntegration process.
      }

      As an option, which is enabled by default, the ImageIntegration process can perform signal and noise evaluations for the integrated image, generating the same metadata as the ImageCalibration and Debayer processes. Currently the ImageIntegration tool provides a reduced set of parameters to control these evaluations; the rest of parameters governing these algorithms are left with default values.

      \figure {

         \image[width:501px,height:auto,border:1px solid gray,marginRight:32px,float:left] II_SignalAndNoiseEvaluation.png

         \figtag The \e {Signal and Noise Evaluation} section of the ImageIntegration tool.
      }

      As for the DrizzleIntegration process, ImageIntegration will include image weights in updated .xdrz files, which DrizzleIntegration will apply to its input images to generate a weighted, drizzle integrated image.

      \note { \s Important--- ImageIntegration expects all signal and noise evaluation metadata items to be present in its input images. This can only happen if the images have been calibrated, and demosaiced when appropriate, with current versions of our ImageCalibration and Debayer tools. When signal and/or noise evaluation metadata items are not available for an image, the ImageIntegration process computes them directly from input pixels. In such case, given that the image has been interpolated for registration (and demosaicing if appropriate), the computed items will be inaccurate (especially noise estimates) and hence the derived PSFSW and PSF SNR values can also be inaccurate, or even wrong in extreme cases. Appropriate warning messages are shown when this happens. }
   }

   \subsection { Metadata } {

      The following table describes the FITS header keywords generated by current versions of the ImageCalibration and Debayer processes when the signal and noise evaluation tasks are enabled. The suffix \e nn in a keyword name represents a zero-padded, zero-based integer channel index. For example, the keyword name 'NOISE00' corresponds to the noise estimate calculated for the first channel (monochrome or red) of the image.

      \table[caption,header,width:100\%] {
         { FITS Keywords Generated for Signal and Noise Evaluation }
         { \center {Keyword name} \center Type \center Description }
         { \center {PSFFLX\e{nn}} \center Real { Sum of PSF flux estimates } }
         { \center {PSFFLP\e{nn}} \center Real { Sum of squared PSF flux estimates } }
         { \center {PSFMFL\e{nn}} \center Real { Sum of mean PSF flux estimates } }
         { \center {PSFMFP\e{nn}} \center Real { Sum of mean squared PSF flux estimates } }
         { \center {PSFMST\e{nn}} \center Real { M* mean background estimate } }
         { \center {PSFNST\e{nn}} \center Real { N* noise estimate } }
         { \center {PSFSGN\e{nn}} \center Integer { Number of valid PSF flux estimates } }
         { \center {PSFSGTYP} \center Character { PSF type used for signal estimation } }
         { \center {NOISE\e{nn}} \center Real { Noise estimate } }
         { \center {NOISEF\e{nn}} \center Real { Fraction of noise pixels (MRS algorithm only) } }
         { \center {NOISEL\e{nn}} \center Real { Noise scaling factor for the SNR estimator, low pixels } }
         { \center {NOISEH\e{nn}} \center Real { Noise scaling factor for the SNR estimator, high pixels } }
         { \center {NOISEA\e{nn}} \center Character { Noise evaluation algorithm } }
      }

      Currently no XISF properties are being generated for signal and noise evaluation because we are still designing their implementation (namespace and property identifiers, as well as their codification in the involved processes). As soon as this work is completed we'll update this document to include complete information on the corresponding XISF properties, which will take precedence, as is customary, over FITS keywords.
   }
}
